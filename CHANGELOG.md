# Changelog

All notable changes to this project will be documented in this file.

## Release 0.4.12

Minor bugfix release

- Fix ill-formed version gate for Puppet 4.x and puppetlabs-apt

## Release 0.4.11

**Features**

**Bugfixes**

- Remove claim for Windows® support, added by `pdk convert`
- Do claim Puppet 7.x support

## Release 0.4.10

**Features**

**Bugfixes**

- Remove claim for Oracle®™ “Linux” support, added by `pdk convert`

## Release 0.4.9

**Features**

**Bugfixes**

- Quash silly warning about `is_string()` going out of fashion

**Known Issues**
