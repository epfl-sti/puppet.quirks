class quirks::private::puppet_module_versions() {
  # Declare that a module is incompatible between different versions of
  # Puppet.
  #
  # === Parameters:
  #
  # $title: The name of the module, e.g. "puppetlabs-ntp"
  #
  # $req_3x: The version requirement to apply for Puppet 3.x (e.g. "< 5.0.0")
  #
  # $req_4x: The version requirement to apply for Puppet 4.x (e.g. "> 5.0.0")
  #
  # $req_5x: The version requirement to apply for Puppet 5.x (e.g. ">
  # 5.1.0"). If omitted, same as $req_4x. The empty string means
  # there is no particular requirement for Puppet 5.x for this module.
  #
  # $req_6x: The version requirement to apply for Puppet 6.x (e.g. ">
  # 5.1.0"). If omitted, same as $req_5x. The empty string means
  # there is no particular requirement for Puppet 6.x for this module.
  #
  # $req_7x: The version requirement to apply for Puppet 7.x (e.g. ">
  # 5.1.0"). If omitted, same as $req_6x. The empty string means
  # there is no particular requirement for Puppet 7.x for this module.
  #
  # $req_8x: The version requirement to apply for Puppet 8.x (e.g. ">
  # 5.1.0"). If omitted, same as $req_7x. The empty string means
  # there is no particular requirement for Puppet 8.x for this module.
  #
  # $req_9x: The version requirement to apply for Puppet 9.x (e.g. ">
  # 5.1.0"). If omitted, same as $req_8x. The empty string means
  # there is no particular requirement for Puppet 9.x for this module.
  #
  # === Actions:
  #
  # * Upgrade or downgrade $title as appropriate
  define incompatible_module(
    $req_3x,
    $req_4x,
    $req_5x = undef,
    $req_6x = undef,
    $req_7x = undef,
    $req_8x = undef,
    $req_9x = undef
  ) {
    $_req_5x = $req_5x ? { undef => $req_4x,  default => $req_5x }
    $_req_6x = $req_6x ? { undef => $_req_5x, default => $req_6x }
    $_req_7x = $req_7x ? { undef => $_req_6x, default => $req_7x }
    $_req_8x = $req_8x ? { undef => $_req_7x, default => $req_8x }
    $_req_9x = $req_9x ? { undef => $_req_8x, default => $req_9x }

    $puppet_module_versions = parsejson($::puppet_module_versions_json)
    $_current_version = $puppet_module_versions[$title]
    if (versioncmp($::puppetversion, '10') > 0) {
      fail("Cannot deal with version ${::puppetversion} of Puppet")
    } elsif (versioncmp($::puppetversion, '9') > 0) {
      $_verclass = "9.x"
      $_req = $_req_9x
    } elsif (versioncmp($::puppetversion, '8') > 0) {
      $_verclass = "8.x"
      $_req = $_req_8x
    } elsif (versioncmp($::puppetversion, '7') > 0) {
      $_verclass = "7.x"
      $_req = $_req_7x
    } elsif (versioncmp($::puppetversion, '6') > 0) {
      $_verclass = "6.x"
      $_req = $_req_6x
    } elsif (versioncmp($::puppetversion, '5') > 0) {
      $_verclass = "5.x"
      $_req = $_req_5x
    } elsif (versioncmp($::puppetversion, '4') > 0) {
      $_verclass = "4.x"
      $_req = $req_4x
    } elsif (versioncmp($::puppetversion, '3') > 0) {
      $_verclass = "3.x"
      $_req = $req_3x
    } else {
      fail("Cannot deal with version ${::puppetversion} of Puppet (too ancient)")
    }

    if ($_req != undef and $_req != "") {
      if (($_current_version == undef) or
          ('not up-to-date' == inline_template("<%= Gem::Dependency.new('', @_req).match?('', @_current_version) ? 'up-to-date': 'not up-to-date' %>"))) {
        $_command = "puppet module uninstall --force ${title}; puppet module install ${title} --version '${_req}'"
        exec { $_command :
          path => $::path
        }
      }
    }
  }
}
