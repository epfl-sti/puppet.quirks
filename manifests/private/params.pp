class quirks::private::params {
  # This may be evaluated with a bogus (stringified) value for
  # $::os; see ./unbreak_structured_facts.pp

  # There used to be an is_string function in stdlib, but at some
  # time around version 7.x it went out of fashion for some kind
  # of new-fangled type matching that for sure won't work in
  # Puppet 3.x, and probably will fall out of fashion again by the
  # time Puppet 9.x rolls out or whatever.
  $_os_is_string = inline_template('<%= (@os.instance_of? String) ? "yes" : "no" %>')
  case $_os_is_string {
    "yes":    {
      $os_is_string = true
      $os = parsejson(inline_template("<%= eval(@os).to_json %>")) }
    default: {
      $os_is_string = false
      $os = $::os
    }
  }
}
