# Ubuntu 16.04 has facter (version 2.4.6) configured to print out
# Ruby-encoded facts, but Puppet (version 3.8.5) doesn't parse them.
# Likewise in RedHat 7.9 with Puppet 3.8.7 and facter 2.4.6
# Fix is in puppet.conf as per
# https://github.com/open-io/puppet-openiosds/issues/1#issue-125610558

class quirks::private::subquirks::unbreak_structured_facts
inherits ::quirks::private::params {
  $_os = $::quirks::private::params::os
  if ($::quirks::private::params::os_is_string) {
    exec { "Ensure stringify_facts=false is present in section [main] of ${main_puppet_conf}":
      path => $::path,
      command => 'true ; set -e -x;
                  configfile="$(puppet config print|sed -ne "s/^config = //p")";
                  if grep "\\[main\\]" $configfile; then
                    sed -i "/^\\[main\\]/a stringify_facts=false" $configfile;
                  else
                    (echo "[main]"; echo stringify_facts=false) >> $configfile;
                  fi'
    }  # exec
  }
}  # class
